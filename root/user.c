/**************************************************************************//**
 * @file user.c
 * @brief handle all data in user data segment
 * @version 1.0.0
 ******************************************************************************
 *
 ******************************************************************************/
#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <intrinsics.h>
#include <em_msc.h>
#include "main.h"
#include "crc.h"
#include "user.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define USERDATA_BASE     (0x0FE00000UL) /**< User data page base address */
#define USER_PAGE_WORDS 128
#define USER_PAGE_BYTES (128*4)

#define USERDATA  ((uint32_t *) USERDATA_BASE)
#define SERIALNUM   0
#define FIRST_START 1
#define SERIALCRC   (USER_PAGE_WORDS - 1)


/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/
 
/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/
/***************************************************************************//**
 * @brief read serial number stored in flash if crc doesn't match rewrite serial
 *                            number.
 *   
 * @param[in] serialNum - default serial number 
 *
 * @return  serial number of device
 ******************************************************************************/

uint32_t userGetSerialNum(uint32_t serialNum)
{
  uint32_t storedSerNum = USERDATA[SERIALNUM];
  uint32_t storedCRC = USERDATA[SERIALCRC];
  uint32_t userPageCopy[USER_PAGE_WORDS]; 
  
  for(int i = 0; i < USER_PAGE_WORDS; i++)userPageCopy[i] = USERDATA[i];
  
  uint32_t realCRC = crc32((uint8_t*)userPageCopy,USER_PAGE_BYTES-4);
  uint32_t newCRC;
  if(( realCRC != storedCRC)||(storedSerNum == 0xFFFFFFFFUL) ||(storedSerNum == 0))
       /* if no serial number stored*/
       /* write new serial number and crc */
  {
    userPageCopy[SERIALNUM] = serialNum;
    newCRC = crc32((uint8_t*)userPageCopy,USER_PAGE_BYTES-4);
    userPageCopy[SERIALCRC] = newCRC;
    MSC_ErasePage(USERDATA);
    // Rewrite user page with new data
    MSC_WriteWord(USERDATA, userPageCopy, USER_PAGE_BYTES);
    storedSerNum = serialNum;   
  }
  return storedSerNum;
}

/***************************************************************************//**
 * @brief reset first start byte to show that MCU was just reprogrammed 
 *   
 * @param[in] none
 *
 * @return  none
 ******************************************************************************/
void userResetFirstStart(void)
{
  uint32_t userPageCopy[USER_PAGE_WORDS]; 
  for(int i = 0; i < USER_PAGE_WORDS; i++)userPageCopy[i] = USERDATA[i];
  userPageCopy[FIRST_START] = 0;
  uint32_t newCRC = crc32((uint8_t*)userPageCopy,USER_PAGE_BYTES-4);
  userPageCopy[SERIALCRC] = newCRC;
  MSC_ErasePage(USERDATA);
  // Rewrite user page with new data
  MSC_WriteWord(USERDATA, userPageCopy, USER_PAGE_BYTES);
}

/***************************************************************************//**
 * @brief set first start byte to show that no need to send init message
 *   
 * @param[in] none
 *
 * @return  none
 ******************************************************************************/
void userSetFirstStart(void)
{
  uint32_t userPageCopy[USER_PAGE_WORDS]; 
  for(int i = 0; i < USER_PAGE_WORDS; i++)userPageCopy[i] = USERDATA[i];
  userPageCopy[FIRST_START] = 1;
  uint32_t newCRC = crc32((uint8_t*)userPageCopy,USER_PAGE_BYTES-4);
  userPageCopy[SERIALCRC] = newCRC;
  MSC_ErasePage(USERDATA);
  // Rewrite user page with new data
  MSC_WriteWord(USERDATA, userPageCopy, USER_PAGE_BYTES);
}

/***************************************************************************//**
 * @brief read first start byte if set return  1, if not then return 0
 *   
 * @param[in] nonr
 *
 * @return  0 or 1
 ******************************************************************************/
uint8_t userIsFistStart(void)
{
  uint32_t storedFirstStart = USERDATA[FIRST_START];
  uint32_t storedCRC = USERDATA[SERIALCRC];
  uint32_t userPageCopy[USER_PAGE_WORDS]; 
  
  for(int i = 0; i < USER_PAGE_WORDS; i++)userPageCopy[i] = USERDATA[i];
  uint32_t realCRC = crc32((uint8_t*)userPageCopy,USER_PAGE_BYTES-4);
  
  if((storedFirstStart == 0)&&(storedCRC == realCRC))return 0;
  
  return 1;
}
/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/