/***************************************************************************//**
 * @file emul.h
 * @brief prepare message for sending, control the number of sensor
 * @version 0.0.1
 *******************************************************************************
 ******************************************************************************/
 
#ifndef EMUL_H
#define EMUL_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************//**
 * @addtogroup EMUL
 * @{
 ******************************************************************************/
 /*******************************************************************************
 *******************************   STRUCTS   ***********************************
 ******************************************************************************/
 
 /*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
 
 /*******************************************************************************
 *****************************   PROTOTYPES   **********************************
 ******************************************************************************/
/***************************************************************************//**
 * @brief create message for sending
 *   
 * @param[in] counter - seconds counter
 *  
 * @param[in] data - reference to massive for message
 *
 * @return   true if time to send message
 *   
 ******************************************************************************/
void emulMessage(uint32_t counter,uint8_t* data, uint8_t dataLen);
/*******************************************************************************
 * @brief address setter
 *   
 * @param[in] address - address of sensor
 *  
 ******************************************************************************/
void emulSetAddr (uint32_t address);
/*******************************************************************************
 * @brief inner temperature setter
 *   
 * @param[in] temperature - inner temperature
 *  
 ******************************************************************************/
void emulSetInT (uint8_t temperature);
/*******************************************************************************
 * @brief outer temperature setter
 *   
 * @param[in] temperature - inner temperature
 *  
 ******************************************************************************/
void emulSetOutT (uint16_t temperature);
/*******************************************************************************
 * @brief activity setter
 *   
 * @param[in] activity - activity value
 * @param[in] index - part of hour 1 - first 15 minutes, 4 - last 15 minutes
 ******************************************************************************/
void emulSetActivity (uint8_t activity, uint8_t index);
/*******************************************************************************
 * @brief condition setter
 *   
 * @param[in] condition - cow condition (quiet, rumination, chewing, not defined)
 *  
 ******************************************************************************/
void emulSetCond (uint8_t condition);
/*******************************************************************************
 * @brief auxiliary1 condition setter
 *   
 * @param[in] condition - cow condition (quiet, rumination, chewing, not defined)
 *  
 ******************************************************************************/
void emulSetCondAux1 (uint8_t condition);
/*******************************************************************************
 * @brief auxiliary2 condition setter
 *   
 * @param[in] condition - cow condition (quiet, rumination, chewing, not defined)
 *  
 ******************************************************************************/
void emulSetCondAux2 (uint8_t condition);
/*******************************************************************************
 * @brief activity setter for testing Azure
 *   
 ******************************************************************************/
void emulSetActivityTest(void);
/*******************************************************************************
 * @brief activity setter for testing Azure
 *   
 ******************************************************************************/
void emulSetCondTest(void);
/***************************************************************************//**
 * @brief create message for sending
 *   
 * @param[in] x,y,z - data from accellerometer 
 *  
 * @param[in] samples - number of samples to send to accelerometer
 *
 * @return   address of array with raw data
 *   
 ******************************************************************************/

uint8_t* emulGetRawData(int16_t *x, int16_t *y, int16_t *z, uint8_t samples);


 
/** @} (end addtogroup EMUL) */

#ifdef __cplusplus
}
#endif

#endif /* EMUL_H */