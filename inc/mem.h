
void memTest(void);
//erase whole chip (60s)
void memChipErase(void);

/******************************************************************
writes 256 bytes to the erased memory
addr - 0 - 0x3FFFFFF the last address byte should be 0
buf - pointer to the RAM buffer
time of programming = 3.2 - 10mS
******************************************************************/

void memWritePage (uint32_t addr, uint8_t *buf);

/*************************************************************************
reads status registers 
returns true if write enable false - write disable
**************************************************************************/

bool memIsWriteEn(void);

/*************************************************************************
reads status registers 
returns true if write in progress false - no write in progress
**************************************************************************/
bool memIsWriteInProgress(void);

/*******************************************************************
read memory
*******************************************************************/
void memReadBuf (uint32_t addr, uint8_t *buf, uint32_t len) ;