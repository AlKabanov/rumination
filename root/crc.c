/**************************************************************************//**
 * @file crc.c
 * @brief name speaks itself
 * @version 1.0.0
 ******************************************************************************
 *
 ******************************************************************************/

#include <em_cmu.h>
#include "crc.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define WIDTH (8*4)
#define TOPBIT (1 << (WIDTH-1))
#define POLYNOMIAL (0x104C11DB7)

/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/
 
/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/
uint32_t crc_table(uint8_t n);
/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/
/*******************************************************************************
 * @brief calculate 32 bits crc
 *   
 * @param[in] buffer - array of data to calculate crc
 * @param[in] length - number of bytes to calculate.
 * @return    32 bits crc
 ******************************************************************************/
uint32_t crc32(const uint8_t *buffer, uint32_t length)
{
    uint32_t crc = 0xFFFFFFFF;
    for(int i = 0; i < length ;i++)
    {
      crc = crc_table(buffer[i] ^ ((crc >> 24) & 0xFF))^(crc << 8);
    }
    return crc ^ 0xFFFFFFFF;
}

/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/
uint32_t crc_table(uint8_t n)
{
    uint32_t c = ((uint32_t)n) << (WIDTH - 8);
    for(int i=8; i > 0; i--)
    {
        if(c & (uint32_t)TOPBIT)c = (c<<1) ^ POLYNOMIAL;    
        else c=c<<1;
    }
    return c;
}
