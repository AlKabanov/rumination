
/***************************************************************************//**
 * @brief
 *   Initialize the UART peripheral.
 *
 * @param[in] speed
 *   The baudrate in bps.
 *
 * @param[in] rxEnable
 *   Enable flag.
 *
 * 
 ******************************************************************************/
void uartInit(uint32_t baudRate, bool rxEnable);

void uartSend(uint8_t byte);
void uartSendArr(uint8_t *data, uint8_t len);
/***************************************************************************//**
 * @brief
 *   Receive a character from the UART.
 *
 * @return
 *   The received character if one is available, -1 otherwise.
 ******************************************************************************/
int uartGetChar(void);
/*****************************************************************************
send data to datalogger
******************************************************************************/
void uartDataLogger(readStruct_t * toSend);