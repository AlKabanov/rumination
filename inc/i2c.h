
void i2cMC3635Init(void);

//read data from accelerometer
void i2cAccDataRead(int16_t *x, int16_t *y, int16_t *z, uint8_t samples);

/***********************************************************************
check FIFO interrupt flag
***********************************************************************/
bool i2cGetFIFOintFlag(void);
/*******************************************************************************
*
*       Form array consisted from 28 clasters by 5 bytes (40bits)
*       one claster is 3 by 13bits data and LSB = 0
*
*******************************************************************************/
void i2cFormDataArr(uint8_t *data, readStruct_t *fromMC3635, uint8_t numClasters, uint8_t temperature, int32_t temperature1, int32_t temperature2);