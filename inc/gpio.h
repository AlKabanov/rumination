

#define PIN_SPI_CS                13			//CSN = PD3
#define PORT_SPI_CS               gpioPortB


#define PIN_SPI_X_CS          4
#define PORT_SPI_X_CS         gpioPortD

#define RST_PORT gpioPortC
#define RST_BIT  13	
#define LoRaRST_EN      GPIO_PinModeSet(RST_PORT, RST_BIT, gpioModePushPull, 0)
#define LoRaRST_ON      GPIO_PinOutSet(RST_PORT, RST_BIT)
#define LoRaRST_OFF     GPIO_PinOutClear(RST_PORT, RST_BIT)

#define TEMP_PWR_PORT gpioPortF
#define TEMP_PWR_BIT  3
#define TEMP_PWR_EN   GPIO_PinModeSet(TEMP_PWR_PORT, TEMP_PWR_BIT, gpioModePushPull, 0)
#define TEMP_PWR_ON   GPIO_PinOutSet(TEMP_PWR_PORT, TEMP_PWR_BIT)
#define TEMP_PWR_OFF  GPIO_PinOutClear(TEMP_PWR_PORT, TEMP_PWR_BIT)

#define I2C_PWR_PORT gpioPortA
#define I2C_PWR_BIT  8
#define I2C_PWR_EN   GPIO_PinModeSet(I2C_PWR_PORT, I2C_PWR_BIT, gpioModePushPullDrive, 0);\
                      GPIO_DriveModeSet(I2C_PWR_PORT,gpioDriveModeHigh)

#define I2C_PWR_ON   GPIO_PinOutSet(I2C_PWR_PORT, I2C_PWR_BIT)
#define I2C_PWR_OFF  GPIO_PinOutClear(I2C_PWR_PORT, I2C_PWR_BIT)


                        
//sx1276 interrupt
#define RF_DIO0_PORT gpioPortC
#define RF_DIO0_PIN 12
#define RF_DIO0_MASK (1<<RF_DIO0_PIN)

#ifdef OLD_BOARD                       
    // mc3635 interrupt
    #define X_INT_PORT gpioPortD
    #define X_INT_PIN 5
    #define X_INT_MASK (1<<X_INT_PIN)
#else 
    // mc3635 interrupt
    #define X_INT_PORT gpioPortA
    #define X_INT_PIN 9
    #define X_INT_MASK (1<<X_INT_PIN)
#endif
                        
/*******************************************************************************
 *************************   TYPEDEFS   ****************************************
 ******************************************************************************/

/** @addtogroup GPIO_TYPEDEFS Typedefs
 * @{ */

/** Interrupt process callback function */
typedef void (*GPIO_callback_t)(void);

/** GPIO IRQ hook structure */
typedef struct {
	GPIO_callback_t callback;	///< callback function to handle this GPIO IRQ
	uint32_t mask;					///< mask to apply for matching this GPIO IRQ
} GPIO_hook_t;

typedef enum{
  rfDIO0 = 0,
  mc3635INT  = 1,
  hookSize,
}gpioInt_t;

/** @} */

/***************************************************************************//**
 * @brief
 *   Register a callback function for processing interrupts matching a given bit mask.
 *
 * @ param[in] type
 *   The type of callback to set up.
 *
 * @param[in] callback
 *   Pointer to callback function called when an interrupt matching the mask is
 *   received.
 *
 * @param[in] mask
 *   Mask for testing a received even interrupts against.
 ******************************************************************************/
static __INLINE void GPIO_SetCallback(gpioInt_t type, GPIO_callback_t callback, uint32_t mask)
{
	extern GPIO_hook_t GPIO_Hooks[];

	GPIO_Hooks[type].callback = callback;
	GPIO_Hooks[type].mask = mask;
}



void gpioSetup(void);

void gpioTEMPPWRON(void);
void gpioTEMPPWROFF(void);
void gpioI2CPWRON(void);
void gpioI2CPWROFF(void);


// set radio RST pin to given value (or keep floating!)
void gpioRST(uint8_t val);

// val ==1  => tx 1, rx 0 ; val == 0 => tx 0, rx 1
void gpioRXTX (uint8_t val);

// set radio NSS pin to given value
void gpioNSS (uint8_t val);


// set axcelerometer NSS pin to given value
void gpioNSS_X (uint8_t val);

bool gpioGetSleepFlag(void);