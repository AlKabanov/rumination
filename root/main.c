/**************************************************************************//**
 * @file
 * @brief firmware for necksensor for finding rumination detection algorithm
 * @version 1.0.1
 ******************************************************************************
 *
 ******************************************************************************/


#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"
//#include "mc3635SPI.h"

#include "main.h"
#include "gpio.h"
#include "rtc.h"
#include "spi.h"
#include "radio.h"
#include "adc.h"
#include "uart.h"
//#include "mc3635.h"
#include "i2c.h"
#include "check.h"
#include "linear.h"
#include "emul.h"
#include "act.h"
#include "otaprog.h"
#include "user.h"
#include "rumination.h"

//#define LORA_TEST 
#define MC3635_TEST
//#define TEST_AZURE
//#define TEST_RUM
//#define TEST_OTAPROG
#define FORSE_NUM
//#define   OLD_BOARD

//#define FAST
#define NORMAL
#define PERIOD_IN_MINUTES 10
//#define PERIOD_IN_MINUTES 3
#define CORRECTION 328

#define ONE_SEC  (32768)
#define TWO_SEC  ((ONE_SEC*2)-1)
#define MSEC_500 ((ONE_SEC/2)-1)
#define MSEC_2 ((ONE_SEC/500)-1)

#define VER 8
#define DEFAULT_SN 6
   

   

#ifdef FAST
      #define PERIOD_MIN 10
      #define MINUTE (ONE_SEC*10 - 1)
#endif
#ifdef NORMAL
      #define PERIOD_MIN PERIOD_IN_MINUTES
      #define MINUTE ((ONE_SEC*60 - 1) - CORRECTION)
#endif






uint8_t data[32];               // 2 - crc
receive_t RADIO_IN = {20,0,0};	//default period = 20mS, dataPtr = 0,lastSent=0,dataValid=0
states_t state = NORMAL_STATE;
readStruct_t fromMC3635;        //, tempFromMC3635;
uint32_t sendCounter = 0, minCounter = 0;
uint32_t minutesCounter = 0, time;
uint32_t serialNumber;
#ifdef TEST_OTAPROG
  uint8_t restart = NO_RESTART;
#else
  uint8_t restart = RESTART;
#endif


/*******************************************************************************
private function prototypes
*******************************************************************************/
void sleepMode(void);
void tests(void);
void initMessage(void);

uint32_t XTestCnt = 0, YTestCnt = 0, ZTestCnt = 0;
/*
int16_t testAcselData[3][PAGE_SAMPLES] = {{-1000,-989,-980,-979,-978,-977,-976,-975,-974,-973,-972,-971,-970,-969,-968,-967,-966,-965,-964,-963,-962,-961,-960,-959,-958,-957,-956,-955},
                                         {100,101,106,110,100,99,87,80,70,60,50,40,30,20,10,0,-10,-20,15,10,11,20,23,28,25,26,27,28},
                                         {-1,0,1,-1,-100,50,60,70,80,90,100,110,111,100,90,45,40,35,30,25,20,2,-10,-100,-200,-300,-400,-500}};*/

/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/
uint8_t *debArr;
void clockLoop(void)
{
 
    
//  radioRXCon(STANDBY_BAND);
  emulSetAddr(serialNumber);
  rtcSetWakeUp(MINUTE);  //sleep for 1 min
  time = rtcGetMS();
  initMessage();
  radioSleep();
  
  
  while (1)
  {
     if(i2cGetFIFOintFlag() == true)
     {
        i2cAccDataRead(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES); //read from FIFO 28 samples
        actAverage(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES);
        actAverage2(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES);
        ruminationState(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES);
        if(state == RAW_DATA_STATE)
        {
          radioTest(emulGetRawData(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES), (PAGE_SAMPLES+1)*3+1, 10, false, txBand(serialNumber));
          //radioTest(emulGetRawData(testAcselData[0],testAcselData[1],testAcselData[2],PAGE_SAMPLES), (PAGE_SAMPLES+1)*3, 10, false, txBand(serialNumber));
        }
     }
     if(time < rtcGetMS())  //one minute passed
     {  
       minutesCounter++;       
       if(minutesCounter == PERIOD_MIN)
       {
         minutesCounter = 0;
#ifdef TEST_AZURE
         emulSetActivityTest();
         emulSetCondTest();
#else
         emulSetActivity(actGetActivity(PERIOD_MIN),1);
         emulSetActivity(actGetActivity2(PERIOD_MIN),2);
#endif
         actResetActivity();
         actResetActivity2();
         ruminationPrepare();
         emulSetCond(ruminationCalculate1());
         emulSetCondAux1(ruminationCalculate2());
         emulSetCondAux2(ruminationCalculate3());
         
         
         //NVIC_EnableIRQ(GPIO_ODD_IRQn);
         //NVIC_EnableIRQ(GPIO_EVEN_IRQn);
         
         sendCounter++;
         
         emulMessage(sendCounter, data, sizeof(data));  //form message to send to server
         
         radioTest(data, sizeof(data), POWER, false, txBand(serialNumber));
         while(!getTXDoneFlag());          //wait for transmission to finish
         radioRXCon(STANDBY_BAND);      //wait for reprogramm command 
         for(int i = 600000; i > 0 ; i--);//wait 200ms 
         state = getRXDoneFlag()?otaprogRout(restart, MINUTE,&RADIO_IN,serialNumber):NORMAL_STATE;
         if(i2cGetFIFOintFlag() == true)
              i2cAccDataRead(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES); //reset mc3635
  
         radioReset();
         radioSleep();
         //for(int i = 15000; i > 0 ; i--);//wait some time to sleep;
         
       }   
       time = rtcGetMS();
     }
     sleepMode();   
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();

  /* Setup RTC to generate an interrupt */
  rtcSetup();  
  
  serialNumber = userGetSerialNum(DEFAULT_SN);
#ifdef FORSE_NUM  
  serialNumber = 1;
#endif
  /* Setup GPIO*/
  gpioSetup();
  
  spiSetup();
  radioReset();
  gpioI2CPWRON();
//  adcInit();
#ifndef LORA_TEST
  i2cMC3635Init();
#endif
  //MC3635SPIInit();
  //uartInit(9600, true);
  tests();
  /* Main function loop */
  clockLoop();

  return 0;
}

/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/
void sleepMode(void)
{
  //gpioPULSEPWROFF();
  
  RTC_IntClear(RTC_IFC_COMP0);
  RTC_IntDisable(RTC_IEN_COMP0 );
  //NVIC_DisableIRQ(GPIO_ODD_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0 );
  EMU_EnterEM2(true);
  
}

/*******************************************************************************
 * @brief first message to understand wether software is upgraded
 *   01000000 970F0000 00 01 9F 00 00 00000000000001000000000000000000000000 
 *
 ******************************************************************************/
void initMessage(void)
{
  if(userIsFistStart()){
    data[0] = (uint8_t)serialNumber;
    data[1] = 0;
    data[2] = 0;
    data[3] = 0;
    data[4] = VER;
    
    radioTest(data, 8, POWER, false, txBand(serialNumber));
    while(!getTXDoneFlag());          //wait for transmission to finish
    userResetFirstStart();
  }
}
/*******************************************************************************
 * @brief some tests
 *   
 ******************************************************************************/
uint16_t test;
#ifdef TEST_RUM
extern int16_t testArr[];
extern uint32_t testLength;
extern uint32_t stateSize;
float in[] = {-1.5,-1.54,-1.49,-0.54,-0.49,1.55,1.49,0.49,0.55};
int out[9];
uint8_t testCond1, testCond2, testCond3;
#endif
void tests(void)
{
#ifdef TEST_RUM
  for(int i = 0;i<9;i++)
  {
    if(in[i] > 0)in[i] += 0.5;
    else in[i] -= 0.5;
    out[i] = (int)in[i];
  }
  radioSleep();
  stateSize = testLength/7;
  NVIC_DisableIRQ(GPIO_ODD_IRQn);
  NVIC_DisableIRQ(GPIO_EVEN_IRQn);
  rtcSetWakeUp(ONE_SEC);  //sleep for 1sec 
  while(1)
  {
    for(int i = 0; i < testLength; i += PAGE_SAMPLES)
    {
      ruminationState(&testArr[i],&testArr[i],&testArr[i],PAGE_SAMPLES);
      //for(int i = 0; i < 50000;i++)__no_operation();
      //sleepMode(); 
    }
    ruminationPrepare();
    testCond1 = ruminationCalculate1();
    testCond2 = ruminationCalculate2();
    testCond3 = ruminationCalculate3();
    __no_operation();
    emulSetAddr(5);
    emulSetActivity(0x77,1);
    emulSetCond(testCond1);
    emulSetCondAux1(testCond2);
    emulSetCondAux2(testCond3);
    emulMessage(0x44444444, data, sizeof(data));  //form message to send to server
    __no_operation();
  }
#endif
    //gpioLDOOn();
#ifdef LORA_TEST
#define TEST_DATA_SIZE 120
  uint8_t testData[TEST_DATA_SIZE];
  data[0] = 1;
  data[1] = 0;
  data[2] = 0;
  
  uint8_t i = 0;
  while(1)
  {
      data[0] = i;
      testData[TEST_DATA_SIZE - 1] = i;
      i++;
     if(i == 10)i=5;
     if(i%2) test = radioTest(data, 32,12,false,band3);
     else radioTest(data, 36,12,false,band0);
//      test = radioTest(testData, TEST_DATA_SIZE,12,false,band3);
      rtcWait(500);
      radioSleep();
      rtcWait(1000);
    
  }
  
#endif
#ifdef MC3635_TEST
  while(1)
  {
    if(i2cGetFIFOintFlag() == true)
    {
      i2cAccDataRead(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES); //read from FIFO 28 samples
      
    }
  }
#endif

}
