/***************************************************************************//**
 * @file
 * @brief I2C MC3635 routines
 * @author Kabanov
 * @version 2.0.1
  ******************************************************************************/

#include <em_i2c.h>
#include <em_cmu.h>
#include <em_gpio.h>
#include <intrinsics.h>
#include "main.h"
#include "gpio.h"
#include "i2c.h"
#include "mc3635.h"


#define SDA_PORT gpioPortB
#define SDA_BIT  11
#define SCL_PORT gpioPortB
#define SCL_BIT  12

#define I2Cx I2C1

#define MC3635_ADDR (0x6C << 1)//(0x6C << 1) 


static int writeReg(uint8_t regAddr, uint8_t data);
static uint8_t readReg(uint8_t regAddr);
static void reset(void);
static void SetWakePowerMode(MC3635_power_t pm);
static void SetSniffPowerMode(MC3635_power_t pm);
static void SetCWakeSampleRate(MC3635_cwake_ODR_t sample_rate);
static void SetResolutionCtrl(MC3635_resolution_t resolution);
static void SetRangeCtrl(MC3635_range_t range);


const I2C_Init_TypeDef g_i2cInit = I2C_INIT_DEFAULT;

uint8_t readVal = 0;
int16_t offsetX,offsetY,offsetZ;
static bool FIFOIntFlag = false;

/************************************************************************
interrupt callback functions
************************************************************************/
static void i2cFIFOThresholdInterrupt(void)
{
  FIFOIntFlag = true;
  readVal = readReg( MC3635_REG_STATUS_2);  //clear interrupt
}
/***********************************************************************
check FIFO interrupt flag
***********************************************************************/
bool i2cGetFIFOintFlag(void)
{
  return FIFOIntFlag;
}

//Set the operation mode  
void SetMode(MC3635_mode_t mode)
{
    uint8_t value;
    value = readReg(MC3635_REG_MODE_C);
    value &= 0xf0;
    value |= mode;
    writeReg(MC3635_REG_MODE_C, value);
}



void i2cMC3635Init(void)
{
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(cmuClock_I2C1, true);
    
    GPIO_PinModeSet(SDA_PORT, SDA_BIT, gpioModeWiredAnd, 1);
    GPIO_PinModeSet(SCL_PORT, SCL_BIT, gpioModeWiredAnd, 1);
    
    for(int i = (4655/10)*5; i> 0;i--); //delay 5mS 
    
    /* Enable pins at location 1*/
    I2Cx->ROUTE = I2C_ROUTE_SDAPEN | I2C_ROUTE_SCLPEN | (1 << _I2C_ROUTE_LOCATION_SHIFT);
                  
    I2C_Init(I2Cx, &g_i2cInit);
    
    
    reset();
     for(int i = (4655/10)*5; i> 0;i--); //delay 5mS 
     writeReg(0x1b,0xff);
     SetMode(STANDBY);
     writeReg(0x1b,0xff);
     
     SetWakePowerMode(PRE_POWER);
     SetSniffPowerMode(PRE_POWER);
     SetRangeCtrl(RANGE_2G);
     SetResolutionCtrl(RESOLUTION_12BIT);
     //SetCWakeSampleRate(LP_14_PRE_14);
     SetCWakeSampleRate(ULP_25_LP_28_PRE_28);
     //SetCWakeSampleRate(ULP_50_LP54_PRE_55); ULP_25_LP_28_PRE_28 
     writeReg(MC3635_REG_FIFO_C,0x80);  //fifo reset
     readVal = readReg( MC3635_REG_STATUS_2);  //clear interrupt
     __no_operation();
     for(int i = (4655/10)*5; i> 0;i--); //delay 5mS 
     writeReg(MC3635_REG_FIFO_C,0x20| 0x40 | 28);  //watermark, fifo enable, threshold = 28
     writeReg(MC3635_REG_INTR_C,0x03 | 0x40);  // Int push-pull active high, fifo threshold interrupt enable
     
     // set callback fo interrupt from X_INT  
     GPIO_SetCallback(mc3635INT, i2cFIFOThresholdInterrupt, X_INT_MASK);
     SetMode(CWAKE);
     
     readVal = readReg(MC3635_REG_FIFO_C);
     __no_operation();
     readVal = readReg(MC3635_REG_INTR_C);
     __no_operation();
    
    //offsetX = readReg(MC3635_REG_XOFFL);
    //offsetX +=(((int16_t)readReg(MC3635_REG_XOFFH))<<8)&0x7FFF ;
    //if(offsetX & 0x4000)offsetX |= 0x8000U; 
}


//write bytes register after initialization
static int writeReg(uint8_t regAddr, uint8_t data)
{
  uint8_t dataToSend[2];
  
  I2C_TransferSeq_TypeDef i2cFrame;
	
  i2cFrame.addr = MC3635_ADDR;
  int retCode;
  
  dataToSend[0] = regAddr;
  dataToSend[1] = data;
  
  i2cFrame.flags = I2C_FLAG_WRITE;
  i2cFrame.buf[0].data = dataToSend;
  i2cFrame.buf[0].len = 2;
  
  //initiate transfer
  I2C_TransferInit(I2Cx, &i2cFrame);
  //use polled transfer
  while ((retCode = I2C_Transfer(I2Cx)) == i2cTransferInProgress);
  
  return(retCode);
  
}
//read 6 bytes data reg
static int16_t lastX = 0, lastY=0, lastZ=0;
void i2cAccDataRead(int16_t *x, int16_t *y, int16_t *z, uint8_t samples)
{
    I2C_TransferSeq_TypeDef i2cFrame;
    uint8_t regToRead[100];
    uint8_t regAddr = MC3635_REG_XOUT_LSB;
    FIFOIntFlag = false;
          
    i2cFrame.addr = MC3635_ADDR;
          
    //int retCode;
    for(int i = 0; i < samples; i++)
    {  
      i2cFrame.flags = I2C_FLAG_WRITE_READ;
         
            
      i2cFrame.buf[0].data = &regAddr;
      i2cFrame.buf[0].len = 1;

              
      i2cFrame.buf[1].data = regToRead;
      i2cFrame.buf[1].len = 6;
              
              //initiate transfer
      I2C_TransferInit(I2Cx, &i2cFrame);
              
              //use polled transfer
      while (I2C_Transfer(I2Cx) == i2cTransferInProgress);
      
      
        x[i] = regToRead[0];
        x[i] +=((int16_t)regToRead[1])<<8;
        
        y[i] = regToRead[2];
        y[i] +=((int16_t)regToRead[3])<<8;
        
        z[i] = regToRead[4];
        z[i] +=((int16_t)regToRead[5])<<8;
        if((x[i] > 4000)||(x[i]<-4000))x[i] = lastX;
        else lastX = x[i];
        if((y[i] > 4000)||(y[i]<-4000))y[i] = lastY;
        else lastY = y[i];
        if((z[i] > 4000)||(z[i]<-4000))z[i] = lastZ;
        else lastZ = z[i];
    }
  readVal = readReg( MC3635_REG_STATUS_2);  //clear interrupt
  
}
/*******************************************************************************
*
*       Form array consisted from 28 clasters by 5 bytes (40bits)
*       one claster is 3 by 13bits data and LSB = 0
*       if temperature == 0 send temperatures
*******************************************************************************/
int16_t temp;
uint8_t claster[5];
void i2cFormDataArr(uint8_t *data, readStruct_t *fromMC3635, uint8_t numClasters, uint8_t temperature, int32_t temperature1, int32_t temperature2)  
{ 
  
  uint32_t temperatureMask = 0;
  if(!temperature)temperatureMask |= 0x04000000;
  temperatureMask |= (temperature1 << 13) & 0x03ffe000;
  temperatureMask |= temperature2 & 0x1fff;
  temperatureMask <<= 1;
  
  for(int i = 0; i < numClasters; i++)
  {
    for(int j = 0; j < sizeof(claster);j++)claster[j] = 0;
    temp = fromMC3635->xBuf[i];
    claster[0] |= temp >> 5; //12,11,10,9,8,7,6,5 bits of X
    claster[1] |= temp << 3; //4,3,2,1,0 bits of X
    temp = fromMC3635->yBuf[i];
    claster[1] |= (temp >> 10) & 0x07; //12,11,10 bits of Y
    claster[2] |= temp >> 2;  // 9,8,7,6,5,4,3,2 bits of Y
    claster[3] |= temp << 6;  //1,0 bits of Y
    temp = fromMC3635->zBuf[i];
    claster[3] |= (temp >> 7) & 0x3f;  //12,11,10,9,8,7 bits of Z
    claster[4] |= temp << 1; //6,5,4,3,2,1,0  bits of Z
    if(temperatureMask & (0x08000000 >> i))claster[4] |= 0x01;  //temperature bit in each claster
    for(int j = 0; j < sizeof(claster); j++)data[i*sizeof(claster) + j] = claster[j];
  }
}

//read 1 byte register after initialization

static uint8_t readReg(uint8_t regAddr)
{
  I2C_TransferSeq_TypeDef i2cFrame;
  uint8_t regToRead[1];
  uint8_t regRead;
	
  i2cFrame.addr = MC3635_ADDR;
	
  //int retCode;
  
  i2cFrame.flags = I2C_FLAG_WRITE_READ;
     
	
  i2cFrame.buf[0].data = &regAddr;
  i2cFrame.buf[0].len = 1;

          
  i2cFrame.buf[1].data = regToRead;
  i2cFrame.buf[1].len = 1;
          
          //initiate transfer
  I2C_TransferInit(I2Cx, &i2cFrame);
          
          //use polled transfer
  while (I2C_Transfer(I2Cx) == i2cTransferInProgress);
  
  regRead = regToRead[0];
  //if(regRead & 0x800000 )reg
    
  return(regRead);
   
}

static void reset(void)
{
  writeReg(MC3635_REG_MODE_C, 0x01);  //standby mode
  writeReg(MC3635_REG_RESET, 0x40);
  for(int i = (4655/10)*1; i> 0;i--); //delay 1mS 
  writeReg(0x0D, 0x40);      //I2C interface
  writeReg(0x0f, 0x42);      //initialization
  writeReg(0x20, 0x01);      //initialization
  writeReg(0x21, 0x80);      //initialization
  writeReg(0x28, 0x00);      //initialization
  writeReg(0x1A, 0x00);      //initialization
  
}

uint8_t readDeb1, writeDeb1;
uint8_t readDeb2, writeDeb2;
uint8_t readDeb3, writeDeb3;
uint8_t readDeb4, writeDeb4;
static void SetWakePowerMode(MC3635_power_t pm)
{
     uint8_t value;
     SetMode(STANDBY);
     value = readReg(MC3635_REG_POWER_MODE);
     value &= 0xf8;
     value |= pm;
     writeDeb1 = value;
     writeReg(MC3635_REG_POWER_MODE, value);
     readDeb1 = readReg(MC3635_REG_POWER_MODE);
}

static void SetSniffPowerMode(MC3635_power_t pm)
{
     uint8_t value;
     SetMode(STANDBY);
     value = readReg(MC3635_REG_POWER_MODE);
     value &= 0x8f;
     value |= pm<<4;
     writeDeb2 = value;
     writeReg(MC3635_REG_POWER_MODE, value);
     readDeb2 = readReg(MC3635_REG_POWER_MODE);
}

static void SetCWakeSampleRate(MC3635_cwake_ODR_t sample_rate) 
{   
     SetMode(STANDBY);
     writeReg(MC3635_REG_WAKE_C, sample_rate);
}
//Set the resolution control
static void SetResolutionCtrl(MC3635_resolution_t resolution)
{
     uint8_t value;
     SetMode(STANDBY);
     value = readReg(MC3635_REG_RANGE_C);
     value &= 0x70;
     value |= resolution;
     writeDeb3 = value;
     writeReg(MC3635_REG_RANGE_C, value);
     readDeb3 = readReg(MC3635_REG_RANGE_C);
}
//Set the range control
static void SetRangeCtrl(MC3635_range_t range)
{
    uint8_t value;    
    SetMode(STANDBY);
    value = readReg(MC3635_REG_RANGE_C);
    value &= 0x8f;
    value |= range << 4;
    writeDeb4 = value;
    writeReg(MC3635_REG_RANGE_C, value);
    readDeb4 = readReg(MC3635_REG_RANGE_C);
}