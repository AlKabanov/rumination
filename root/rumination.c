/**************************************************************************//**
 * @file rumination.c
 * @brief module for calculatin rumination, chewing, rest, drinking states
 * @version 1.0.0
 ******************************************************************************
 */
#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
//#include <em_leuart.h>
//#include <em_gpio.h>
#include <intrinsics.h>
#include "main.h"
#include "rumination.h"


/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define AVERAGE 42 
#define SAMPLES_PER_SEC 4
#define OBSERV_PERIOD_SEC 600
#define X_THRESHOLD 500
#define Y_THRESHOLD 500
#define Z_THRESHOLD 500
#define STATE_SIZE (OBSERV_PERIOD_SEC*SAMPLES_PER_SEC)
#define AXES 3
#define RUM_PERIOD_MIN (20*SAMPLES_PER_SEC)
#define RUM_PERIOD_MAX (180*SAMPLES_PER_SEC)
#define X axe[0]
#define Y axe[1]
#define Z axe[2]

#define NCoef 6

   
 /*******************************************************************************
 *******************************   TYPEDEFS   ***********************************
 ******************************************************************************/
typedef struct {
  int  bpfArr[256];
  int averArr[256];  //averaged X
  bool     stateArr[STATE_SIZE];
  double y[NCoef+1];       //filter output
  double x[NCoef+1];       //filter input samples
  uint16_t threshold;
  uint8_t index;
  int stateIndex;
	
} axe_t;

/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/

axe_t  axe[AXES] = {{.threshold = X_THRESHOLD,.index = 0,.stateIndex = 0},
                    {.threshold = Y_THRESHOLD,.index = 0,.stateIndex = 0},
                    {.threshold = Z_THRESHOLD,.index = 0,.stateIndex = 0}};
                          
uint32_t stateSize = STATE_SIZE;
//static uint8_t index = 0;
//static int stateIndex = 0;

/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/
static int ifilter(int newSample, double* y, double* x);
uint8_t calculate(bool* stateArr, int stateIndex, int threshold);

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

/***************************************************************************//**
 * @brief preparation for calculation animal condition
 *   
 * @param[in] x axe of accelerometer reference 
 * @param[in] y
 * @param[in] x
 * @param[in] samples = number of samples
 *
 * @return    
 ******************************************************************************/
#define AXE axe[ii]  
uint8_t rumDebI1;
uint8_t rumDebI2;

void ruminationState(int16_t *x, int16_t *y, int16_t *z, uint8_t samples)
{
    /*fill filtered data array, calculate and fill 42 average array*/
  int16_t* input[3] = {x,y,z};
  int average;
  
  for(uint8_t ii = 0; ii < AXES; ii++)
  {
    for(uint8_t i = 0; i < samples; i++)
    {
      AXE.bpfArr[AXE.index] = ifilter(*(input[ii]+i),AXE.y,AXE.x);
      //AXE.bpfArr[AXE.index] = ffilter((float)*(input[ii]+i),,AXE.y,AXE.x);
      average = 0;
      for(uint8_t k = 0; k < AVERAGE; k++) 
      {average += ABS(AXE.bpfArr[(uint8_t)(AXE.index - k)] - AXE.bpfArr[(uint8_t)(AXE.index - k - 1)]);}
      
      AXE.averArr[AXE.index] = average;///AVERAGE;
      
      if(i%7 == 6) //average in groupes of 1/4 seconds (7 samples)
      {
        average = 0;
        for(uint8_t j = 0; j< 6; j++)average += AXE.averArr[(uint8_t)(AXE.index-j)];
        if(average < AXE.threshold)AXE.stateArr[AXE.stateIndex] = true;
        else AXE.stateArr[AXE.stateIndex] = false;
        //AXE.stateIndex = indexAdd(1, AXE.stateIndex);
        if(AXE.stateIndex < (stateSize - 1))AXE.stateIndex++; //fill state array from 0 to stateSize
      }
      AXE.index++;
      /**debug section**/
      if(AXE.index == 0)
              __no_operation();
    }
  }
  
}
/***************************************************************************//**
 * @brief save counters for rumination calculation
 *
 ******************************************************************************/
int stateIndex = 0;
bool generalArray[STATE_SIZE];

void ruminationPrepare(void)
{
  stateIndex = Z.stateIndex;
  Z.stateIndex = 0; //reset stateIndexes
  X.stateIndex = 0;
  Y.stateIndex = 0;
}

/***************************************************************************//**
 * @brief calculating state of the animal
 *
 * methode N1 use only Z axe  
 * @return animal condition
 ******************************************************************************/
uint8_t ruminationCalculate1(void)
{
  uint8_t condition = calculate(Z.stateArr,stateIndex,stateIndex/2);  
  return condition;
}

/***************************************************************************//**
 * @brief calculating state of the animal
 *
 * methode N2 use X Y Z axes with majority principal
 * @return animal condition
 ******************************************************************************/


uint8_t ruminationCalculate2(void)
{
  for(int i = 0; i < stateIndex; i++)
  {
    if(((int)Z.stateArr[i] + (int)X.stateArr[i] + (int)Y.stateArr[i]) > 1)generalArray[i] = true;
    else generalArray[i] = false;
  }
  uint8_t condition = calculate(generalArray, stateIndex,stateIndex/2);
   
  return condition;
}

/***************************************************************************//**
 * @brief calculating state of the animal
 *
 * methode N2 use Z axe Threshold is higher
 * @return animal condition
 ******************************************************************************/

uint8_t ruminationCalculate3(void)
{
  uint8_t condition = calculate(Z.stateArr,stateIndex,(stateIndex*2)/3);
  return condition;
}

#define RUM_MODE 1
#define NO_MODE 0
#define READY_RUM_MODE 2

uint8_t calculate(bool* stateArr, int stateIndex, int threshold)
{
  uint8_t mode = NO_MODE;
  uint8_t condition = COND_NO;
  uint32_t rumCounter = 0;
  uint32_t quietCounter = 0;
  uint32_t chewCounter = 0;
  uint32_t rumCounterGeneral = 0;
  /*remove occasional ones and zeros*/
  if(stateIndex < 12)return COND_NO;
  for(int i = 5; i < (stateIndex - 6); i++)
  {
    stateArr[i] = !stateArr[i];
    for( int k = i-5; k < i+6;k++)
    {
      if(stateArr[k] != stateArr[i])
      {
        stateArr[i] = !stateArr[i];
        break;
      }
    }
  }
  for(int i = 0; i < (stateIndex - 1); i++)
  {
    switch(mode)
    { 
      case NO_MODE:
        if((stateArr[i] == true)&&(stateArr[i+1] == false)) //shange from 1 to 0
        {
          mode = READY_RUM_MODE;
          rumCounter = 0;
        }
      break;
      case READY_RUM_MODE:
        if(stateArr[i] == false)rumCounter++;
        else
        {
          if((rumCounter > RUM_PERIOD_MIN)&&(rumCounter < RUM_PERIOD_MAX))rumCounterGeneral += rumCounter;
          mode = NO_MODE;
        }
        
      break;
    }
    if(stateArr[i] == true)quietCounter++;
    else chewCounter++;
  }
  
  if(rumCounterGeneral > threshold) condition = COND_RUM;
  else if(quietCounter > threshold) condition = COND_QUIET;
  else if(chewCounter > threshold) condition = COND_CHEW;
  else condition = COND_NO;
 
 
  return condition;
}

/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************/
/***************************************************************************//**
 * @brief by pass filter
 *   
 * @param[in] value input value
 * 
 * @return filtered value
 ******************************************************************************/

#define DCgain 1024

static int ifilter(int newSample, double* y, double* x)
{
  double ACoef[NCoef+1] = { 6233,    0,-18700,     0,18700,    0,-6233};
  double BCoef[NCoef+1] = { 2048,-9928, 20991,-24675,16999,-6513, 1089};
   
    int n;
    double out;
   //shift the old samples
    for(n=NCoef; n>0; n--) 
    {
       x[n] = x[n-1];
       y[n] = y[n-1];
    }
    //Calculate the new output
    x[0] = newSample;
    y[0] = ACoef[0] * x[0];
    for(n=1; n<=NCoef; n++)y[0] += ACoef[n] * x[n] - BCoef[n] * y[n];
    y[0] /= BCoef[0];
   
    out = y[0]/DCgain;
    if(out > 0)out += 0.5;
    else out -= 0.5;
    //return (y[0]/DCgain);
    return (int)out;
}

