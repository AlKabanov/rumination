/**************************************************************************//**
 * @file act.c
 * @brief module for counting activity using accelerometer
 * @version 1.0.0
 ******************************************************************************
 *
 ******************************************************************************/
#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include <intrinsics.h>
//#include "radio.h"
#include "main.h"
#include "act.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define DIVIDER 20
#define DIVIDER_2 10
#define NCoef 3
/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/
 static bool startDiff = false;
 static bool startDiff2 = false;
 uint32_t diffX, diffY, diffZ;
 uint32_t diffX2, diffY2, diffZ2;
 static int16_t prevX, prevY, prevZ;
 static int16_t prevX2,prevY2,prevZ2;
 static double Xout[NCoef+1],Yout[NCoef+1],Zout[NCoef+1]; //output samples
 static double Xin[NCoef+1],Yin[NCoef+1],Zin[NCoef+1]; //input samples
/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/
static int ifilter(double NewSample, double* out, double* in);
/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/
/***************************************************************************//**
 * @brief calculating average and differencial
 *   
 * @param[in] x axe of accelerometer reference 
 * @param[in] y
 * @param[in] x
 * @param[in] samples = number of samples
 *
 * @return    
 ******************************************************************************/
   
void actAverage(int16_t *x, int16_t *y, int16_t *z, uint8_t samples)
{
  int32_t summX = 0, summY = 0, summZ = 0;
  for(int i = 0; i < samples; i++){summX += x[i], summY += y[i], summZ += z[i];}
  summX /=samples, summY /=samples, summZ /=samples;
  if(startDiff == false)
  {
    startDiff = true;
    prevX = summX, prevY = summY, prevZ = summZ;
    diffX = 0, diffY = 0, diffZ = 0;
  }
  diffX += ABS(summX-prevX), diffY += ABS(summY-prevY), diffZ += ABS(summZ-prevZ);
  prevX = summX, prevY = summY, prevZ = summZ;
}
/***************************************************************************//**
 * @brief calculating average and differencial second variant
 *   
 * @param[in] x axe of accelerometer reference 
 * @param[in] y
 * @param[in] x
 * @param[in] samples = number of samples
 *
 * @return    
 ******************************************************************************/
   
void actAverage2(int16_t *x, int16_t *y, int16_t *z, uint8_t samples)
{
  int32_t summX = 0, summY = 0, summZ = 0;
  for(int i = 0; i < samples; i++)
  {
    summX += ifilter(x[i],Xout,Xin);
    summY += ifilter(y[i],Yout,Yin);
    summZ += ifilter(z[i],Zout,Zin);
  }
  summX /=samples, summY /=samples, summZ /=samples;
  if(startDiff2 == false)
  {
    startDiff2 = true;
    prevX2 = summX, prevY2 = summY, prevZ2 = summZ;
    diffX2 = 0, diffY2 = 0, diffZ2 = 0;
  }
  diffX2 += ABS(summX-prevX2), diffY += ABS(summY-prevY2), diffZ += ABS(summZ-prevZ2);
  prevX2 = summX, prevY2 = summY, prevZ2 = summZ;
}
/***************************************************************************//**
 * @brief sending current average acytivity divided by diveder to be uint8_t
 * @param [in] minutes - how many minutes period lasts
 * @return mean activity  per minute
 ******************************************************************************/
uint8_t actGetActivity(uint8_t minutes)
{
  uint32_t activity;
  activity = (diffX + diffY + diffZ)/(3*minutes);  //average activity for 1 minute
  activity = (activity > (DIVIDER*0xFF))?0xff:(activity/DIVIDER);
  return (uint8_t)activity;
  
}
/***************************************************************************//**
 * @brief sending current average acytivity divided by diveder to be uint8_t
 * @param [in] minutes - how many minutes period lasts
 * @return mean activity  per minute
 ******************************************************************************/
uint8_t actGetActivity2(uint8_t minutes)
{
  uint32_t activity;
  activity = (diffX2 + diffY2 + diffZ2)/(3*minutes);  //average activity for 1 minute
  activity = (activity > (DIVIDER_2*0xFF))?0xff:(activity/DIVIDER_2);
  return (uint8_t)activity;
  
}
/***************************************************************************//**
 * @brief clear summs of activities
 *   
 ******************************************************************************/
void actResetActivity(void)
{
  diffX = 0, diffY = 0, diffZ = 0;
}

/***************************************************************************//**
 * @brief clear summs of activities
 *   
 ******************************************************************************/
void actResetActivity2(void)
{
  diffX2 = 0, diffY2 = 0, diffZ2 = 0;
}


/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************/
/***************************************************************************//**
 * @brief low pass filter
 *   
 * @param[in] value input value
 * 
 * @return filtered value
 ******************************************************************************/

static int ifilter(double NewSample, double* out, double* in) {
     double ACoef[NCoef+1] = {
        0.00000130265309843593,
        0.00000390795929530779,
        0.00000390795929530779,
        0.00000130265309843593
    };

    double BCoef[NCoef+1] = {
        1.00000000000000000000,
        -2.95512104614865120000,
        2.91124350478987730000,
        -0.95611140833190200000
    };

    
    int n;

    //shift the old samples
    for(n=NCoef; n>0; n--) {
       in[n] = in[n-1];
       out[n] = out[n-1];
    }

    //Calculate the new output
    in[0] = NewSample;
    out[0] = ACoef[0] * in[0];
    for(n=1; n<=NCoef; n++)out[0] += ACoef[n] * in[n] - BCoef[n] * out[n];
          
    //return out[0];
    
    double iout = out[0];
    if(iout > 0)iout += 0.5;
    else iout -= 0.5;
    return (int)iout;
}
