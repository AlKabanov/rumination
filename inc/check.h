#ifndef CHECK_H
#define CHECK_H

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>

/*******************************************************************************
* @brief
 *   Check if recieved message is wake up messge and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/
bool checkWakeUp(uint8_t address, receive_t *message);

/*******************************************************************************
* @brief
 *   Check if recieved message is go to tx rumination 
 *   messge and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/     
bool checkTXRum(uint8_t address, receive_t *message);
/*******************************************************************************
* @brief
 *   Check if recieved message is stop
 *   messge and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/
bool checkStop(uint8_t address, receive_t *message);
/*******************************************************************************
* @brief
 *   Check if recieved message is "is alive"
 *   messge and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/
bool checkIsAlive(uint8_t address, receive_t *message);
/*******************************************************************************
* @brief
 *   Check if recieved message is first "programm"
 *   message and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/
bool checkProgram1(uint8_t address, receive_t *message);
/*******************************************************************************
* @brief
 *   Check if recieved message is second "programm" 
 *   message and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/
bool checkProgram2(uint8_t address, receive_t *message);
/*******************************************************************************
* @brief
 *   find frequency band for transmition
 *
 * @param[in] address 
 *    addres of transmitter
 * @return
 *   frequency band
********************************************************************************/
band_t txBand(uint8_t address);

#endif /* CHECK_H */