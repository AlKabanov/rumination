#include <em_cmu.h>
#include <em_emu.h>

#include <em_gpio.h>
#include <em_usart.h>
#include <intrinsics.h>
#include "rtc.h"
#include "spi.h"
#include "gpio.h"
#include "mc3635.h"
#include "mc3635SPI.h"

const uint8_t WRITE = 0x40;
const uint8_t READ = 0xC0;
const uint8_t MASK = 0x3F;
static inline void delay(void){
  for(int i = 500; i > 0;i--);
}

static void writeReg (uint8_t addr, uint8_t data ) {
    uint8_t val = addr & MASK;
    val |= WRITE;
    gpioNSS_X(0);
    spiSend(val);
    spiSend(data);
    delay();
    gpioNSS_X(1);
}

static uint8_t readReg (uint8_t addr) {
    uint8_t val =  addr | READ;
    gpioNSS_X(0);
    spiSend(val);
    val = spiSend(0x00);
    delay();
    gpioNSS_X(1);
    return val;
}

static void writeBuf (uint8_t addr, uint8_t *buf, uint8_t len) {
    uint8_t val = addr & MASK;
    val |= WRITE;
    gpioNSS_X (0);
    spiSend(val);
    for (uint8_t i=0; i<len; i++) {
        spiSend(buf[i]);
    }
    delay();
    gpioNSS_X(1);
}

static void readBuf (uint8_t addr, uint8_t *buf, uint8_t len) {
    uint8_t val =  addr | READ;
    gpioNSS_X (0);
    spiSend(val);
    for (uint8_t i=0; i<len; i++) {
        buf[i] = spiSend(0x00);
    }
    delay();
    gpioNSS_X(1);
}

static void reset(void)
{
  writeReg(MC3635_REG_MODE_C, 0x01);  //standby mode
  writeReg(MC3635_REG_RESET, 0x40);
  writeReg(0x1b, 0x00);
  for(int i = (4655/10)*1; i> 0;i--); //delay 1mS 
  writeReg(0x0D, 0x80);      //SPI interface
  writeReg(0x0f, 0x42);      //initialization
  writeReg(0x20, 0x01);      //initialization
  writeReg(0x21, 0x80);      //initialization
  writeReg(0x28, 0x00);      //initialization
  writeReg(0x1A, 0x00);      //initialization
  
}

//Set the operation mode  
static void SetMode(MC3635_mode_t mode)
{
    uint8_t value;
    value = readReg(MC3635_REG_MODE_C);
    value &= 0xf0;
    value |= mode;
    writeReg(MC3635_REG_MODE_C, value);
}


void MC3635SPIInit(void)
{
   reset();
   for(int i = (4655/10)*5; i> 0;i--); //delay 5mS 
   writeReg(0x1b,0xff);
   SetMode(STANDBY);
   writeReg(0x1b,0xff);
   SetMode(STANDBY);
}