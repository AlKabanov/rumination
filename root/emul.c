#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include <intrinsics.h>
#include "main.h"
#include "emul.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define PULSE_NO_CONNECTION 0
#define PULSE_FAILURE 1
#define PULSE_NO_MEASURE 2

//#define BYTEHIGH(v)   (*(((unsigned char *) (&v) + 1)))
//#define BYTELOW(v)  (*((unsigned char *) (&v)))
//#define ACTIVITY_PARTS 4

typedef struct{
  uint32_t address;
  uint32_t counter;
  uint8_t  type;
  uint8_t  cowCond;
  uint8_t  cowActivity[ACTIVITY_PARTS];
  uint8_t  cowCondAux1;
  uint8_t  cowCondAux2;
  uint8_t  outTHigh;
  uint8_t  temp3;
  uint8_t  inT;
  uint8_t  pulse;
  uint8_t  aes128[12];
} message_t;



/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/
message_t message;
static uint32_t sensorAddress;
static uint8_t inT;
static uint16_t outT;
static uint8_t activityArr[ACTIVITY_PARTS];
static uint8_t cowCond = 0, cowCondAux1 = 0, cowCondAux2 = 0;
//static uint8_t messageLen = sizeof(message);
/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

/*******************************************************************************
 * @brief address setter
 *   
 * @param[in] address - address of sensor
 *  
 ******************************************************************************/
void emulSetAddr (uint32_t address)
{
  sensorAddress = address;
}
/*******************************************************************************
 * @brief inner temperature setter
 *   
 * @param[in] temperature - inner temperature
 *  
 ******************************************************************************/
void emulSetInT (uint8_t temperature)
{
  inT = temperature;
}
/*******************************************************************************
 * @brief outer temperature setter
 *   
 * @param[in] temperature - inner temperature
 *  
 ******************************************************************************/
void emulSetOutT (uint16_t temperature)
{
  outT = temperature;
}
/*******************************************************************************
 * @brief condition setter
 *   
 * @param[in] condition - cow condition (quiet, rumination, chewing, not defined)
 *  
 ******************************************************************************/
void emulSetCond (uint8_t condition)
{
  cowCond = condition;
}

/*******************************************************************************
 * @brief auxiliary1 condition setter
 *   
 * @param[in] condition - cow condition (quiet, rumination, chewing, not defined)
 *  
 ******************************************************************************/
void emulSetCondAux1 (uint8_t condition)
{
  cowCondAux1 = condition;
}

/*******************************************************************************
 * @brief auxiliary2 condition setter
 *   
 * @param[in] condition - cow condition (quiet, rumination, chewing, not defined)
 *  
 ******************************************************************************/
void emulSetCondAux2 (uint8_t condition)
{
  cowCondAux2 = condition;
}



/*******************************************************************************
 * @brief activity setter
 *   
 * @param[in] activity - activity value
 * @param[in] index - part of hour 1 - first 15 minutes, 4 - last 15 minutes
 ******************************************************************************/
void emulSetActivity (uint8_t activity, uint8_t index)
{
  if ((index > 0)&&(index < (ACTIVITY_PARTS+1)))
  {
    activityArr[index-1] = activity; 
  }
}


/***************************************************************************//**
 * @brief create message for sending
 *   
 * @param[in] counter - messages counter
 *  
 * @param[in] data - reference to array for message
 *
 * @param[in] dataLen - length of array
 *
 * @return   true if time to send message
 *   
 ******************************************************************************/
void emulMessage(uint32_t counter,uint8_t* data, uint8_t dataLen)
{
 
    message.address = sensorAddress;
    message.counter = counter;
    message.type = 0;
    message.cowCond = cowCond;  
    for(int i = 0; i < ACTIVITY_PARTS; i++)message.cowActivity[i] = activityArr[i];
    message.inT = inT;
    message.outTHigh = BYTEHIGH(outT);
    //message.outTLow = BYTELOW(outT);
    message.cowCondAux1 = cowCondAux1;
    message.cowCondAux2 = cowCondAux2;
    message.temp3 = 0;
    message.pulse = message.cowActivity[1]; 
    for(int i = 0; i < dataLen;i++)data[i] = (*(((uint8_t *) (&message) + i)));  //send bytes from struct to array

}
/*******************************************************************************
 * @brief activity setter for testing Azure
 *   
 ******************************************************************************/
static uint8_t activityTestArr [] ={5,10,20,30,40,50,60,70,80,90,100,150,100,80,60,40,20,10};
void emulSetActivityTest(void)
{
  static uint8_t activityIndex = 0;
  if(++activityIndex == sizeof(activityTestArr))activityIndex = 0;
  activityArr[0] = activityTestArr[activityIndex];
  
}
/*******************************************************************************
 * @brief activity setter for testing Azure
 *   
 ******************************************************************************/
static uint8_t cowCondTestArr[] = {COND_QUIET, COND_CHEW, COND_RUM, COND_RUM, COND_CHEW, COND_CHEW, COND_NO, COND_NO,COND_QUIET,COND_CHEW};
void emulSetCondTest(void)
{
  static uint8_t cowCondIndex = 0;
  if(++cowCondIndex == sizeof(cowCondTestArr))cowCondIndex = 0;
  cowCond = cowCondTestArr[cowCondIndex];
}
/***************************************************************************//**
 * @brief create message for sending
 *   
 * @param[in] x,y,z - data from accellerometer 
 *  
 * @param[in] samples - number of samples to send to accelerometer
 *
 * @return   address of array with raw data
 *   
 ******************************************************************************/
static struct{
  uint8_t rawData[3][PAGE_SAMPLES+1];  
  uint8_t overRanged;
} send;

uint8_t* emulGetRawData(int16_t *x, int16_t *y, int16_t *z, uint8_t samples)
{
  send.rawData[0][0] = x[0] >> 8; //first high byte x
  send.rawData[0][1] = x[0]; //first low byte x
  send.rawData[1][0] = y[0] >> 8; //first high byte y
  send.rawData[1][1] = y[0]; //first low byte y
  send.rawData[2][0] = z[0] >> 8;//first high byte z
  send.rawData[2][1] = z[0];//first low byte z
  if(samples > PAGE_SAMPLES)samples = PAGE_SAMPLES;
  send.overRanged = 0;
  for(int i = 1; i < samples; i++)
  {
    send.overRanged += 3;
    if((x[i]-x[i-1])>127)send.rawData[0][i+1] = 127;
    else if((x[i] - x[i-1])<-128)send.rawData[0][i+1] = 0x80; //-128
    else {
      send.rawData[0][i+1] = x[i]-x[i-1];
      send.overRanged--;
    }
    if((y[i]-y[i-1])>127)send.rawData[1][i+1] = 127;
    else if((y[i] - y[i-1])<-128)send.rawData[1][i+1] = 0x80; //-128
    else {
      send.rawData[1][i+1] = y[i]-y[i-1];
      send.overRanged--;
    }
    if((z[i]-z[i-1])>127)send.rawData[2][i+1] = 127;
    else if((z[i]-z[i-1])<-128)send.rawData[2][i+1] = 0x80; //-128
    else {
      send.rawData[2][i+1] = z[i]-z[i-1];   
      send.overRanged--;
    }
  }
  return &send.rawData[0][0];
}
/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/

