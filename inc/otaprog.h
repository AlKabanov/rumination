/***************************************************************************//**
 * @file otaprog.h
 * @brief reprogramming uc over the air
 * @version 0.0.1
 *******************************************************************************
 ******************************************************************************/
 
#ifndef OTAPROG_H
#define OTAPROG_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************//**
 * @addtogroup OTAPROG
 * @{
 ******************************************************************************/
 /*******************************************************************************
 *******************************   STRUCTS   ***********************************
 ******************************************************************************/
 
 /*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
  typedef enum
{
    NORMAL_STATE,
    RAW_DATA_STATE,
}states_t;
 
 /*******************************************************************************
 *****************************   PROTOTYPES   **********************************
 ******************************************************************************/

/*******************************************************************************
 * @brief received data for flash reprogramming
 * if data valid  then reprograms flash and reset system
 *   
 * @param[in] mode FOR_RESTART - write
 * @return    false - error true - ok
 ******************************************************************************/

bool otaprogProg (uint8_t mode);
/*******************************************************************************
 * @brief check bin file length and save CRC
 *   
 * @param[in] message - reference to received data
 * @return    false - memory allocation error
 *            true  - ok
 ******************************************************************************/
bool otaprogPrepare(uint8_t *message);
/*******************************************************************************
 * @brief memory allocate for bin file usin progLength
 *   
 * @param[in] none
 * @return    true - memory allocation error
 *            false  - memory allocated
 ******************************************************************************/
bool otaprogMalloc(void);
/*******************************************************************************
 * @brief free progArr memory
 * @param[in] none
 * @return    none
 ******************************************************************************/
void otaprogFree(void);
/*******************************************************************************
 * @brief write to progArr any received message check data integrity
 *              set progArrError if needed (CRC error,...)
 *   
 * @param[in] buffer  first byte is counter, 
              messSize - size of data payload ex. counter and crc
 * @return    true - end of data
 *            false - otherwise
 ******************************************************************************/
bool otaprogReadMess(uint8_t *buffer,uint8_t messSize);
/*******************************************************************************
 * @brief check crc and length of received bin file
 *  @param[in] start  pointer to array for crc count 
 *            length - length of array 
 *              crc  - value for compare
 * @return    true - if crc matches to the calculated value
 *            false - otherwise
 ******************************************************************************/
bool otaprogValid(uint8_t* start, uint32_t length,uint32_t crc);

/*******************************************************************************
 * @brief wait for reprogram command, 
 *                              then reprograms flash and reset system
 *   
 * @param[in] mode RESTART - write
 * @param[in] period - if return, setup rtc timer for sleep period
 * @param[in] RADIO_IN data from LoRa
 * @return    state of the sensor
 ******************************************************************************/
states_t otaprogRout(uint8_t mode,uint32_t period,receive_t *RADIO_IN,uint32_t SerialNumber);
 
/** @} (end addtogroup OTAPROG) */

#ifdef __cplusplus
}
#endif

#endif /* OTAPROG_H */