
#ifndef ADC_H
#define ADC_H
/***************************************************************************//**
* @brief
*   Configure ADC usage for measuring input channels with oversampling.
*******************************************************************************/
void adcInit(void);


/***************************************************************************//**
* @brief
* @param 
*   channel number 1 or 2
* @return
*  result of convercion on ADC0_CH7 if param == 2
*   or ADC0_CH6 if param == 1
*******************************************************************************/

int32_t adcGetTemperature(uint8_t);
#endif /* ADC_H */